var rs = require('readline-sync');

const validateNumbers = (x, y) => {
    return (isNaN(x) || isNaN(y)) ? false : true
}

const validateNumber = (x) => {
    return (isNaN(x)) ? false : true
}

let op = rs.question(`silahkan pilih operator:
tambah
kurang
kali
bagi
akar kuadrat
luas persegi
volume kubus
volume tabung
Masukkan pilihanmu: `);

if (op === "tambah") {
    let num1 = rs.question('Masukkan angka : ');
    let num2 = rs.question('Masukkan angka lainnya : ');
    if (!validateNumbers(num1, num2)) {
        console.log("Input harus angka. Silakan mulai ulang program.");
    } else {
        let hasil = Number(num1) + Number(num2);
        console.log(`hasilnya : ${hasil}`);
    }
} else if (op === "kurang") {
    let num1 = rs.question('Masukkan angka : ');
    let num2 = rs.question('Masukkan angka lainnya : ');
    if (!validateNumbers(num1, num2)) {
        console.log("Input harus angka. Silakan mulai ulang program.");
    } else {
        let hasil = Number(num1) - Number(num2);
        console.log(`hasilnya : ${hasil}`);
    }
} else if (op === "kali") {
    let num1 = rs.question('Masukkan angka : ');
    let num2 = rs.question('Masukkan angka lainnya : ');
    if (!validateNumbers(num1, num2)) {
        console.log("Input harus angka. Silakan mulai ulang program.");
    } else {
        let hasil = Number(num1) * Number(num2);
        console.log(`hasilnya : ${hasil}`);
    }
} else if (op === "bagi") {
    let num1 = rs.question('Masukkan angka : ');
    let num2 = rs.question('Masukkan angka lainnya : ');
    if (!validateNumbers(num1, num2)) {
        console.log("Input harus angka. Silakan mulai ulang program.");
    } else {
        let hasil = Number(num1) / Number(num2);
        console.log(`hasilnya : ${hasil}`);
    }
} else if (op === "akar kuadrat") {
    let num1 = rs.question('Masukkan angka : ');
    if (!validateNumber(num1)) {
        console.log("Input harus angka. Silakan mulai ulang program.");
    } else {
        let hasil = Number(Math.sqrt(num1));
        console.log(`hasilnya : ${hasil}`);
    }
} else if (op === "luas persegi") {
    let num1 = rs.question('Masukkan panjang sisi : ');
    if (!validateNumber(num1)) {
        console.log("Input harus angka. Silakan mulai ulang program.");
    } else {
        let hasil = Number(num1 * num1);
        console.log(`hasilnya : ${hasil}`);
    }
} else if (op === "volume kubus") {
    let num1 = rs.question('Masukkan panjang sisi : ');
    if (!validateNumber(num1)) {
        console.log("Input harus angka. Silakan mulai ulang program.");
    } else {
        let hasil = Number(num1 * num1 * num1);
        console.log(`hasilnya : ${hasil}`);
    }
} else if (op === "volume tabung") {
    let num1 = rs.question('Masukkan jari-jari (r) : ');
    let num2 = rs.question('Masukkan tinggi (t) : ');
    if (!validateNumbers(num1, num2)) {
        console.log("Input harus angka. Silakan mulai ulang program.");
    } else {
        let hasil = Number(Math.PI * num1 * num1 * num2);
        console.log(`hasilnya : ${hasil}`);
    }
} else {
    console.log("Operasi tidak ada,silahkan jalankan program kembali");
}

